;; Copyright 2010, 2011 Elias Pipping <pipping@exherbo.org>
;; Distributed under the terms of the GNU General Public License v2

(require 'font-lock)
(require 'sh-script)

;;; Font Lock
(defvar eclectic-font-lock
  (eval-when-compile
    (list (list (regexp-opt '("append_config" "arch" "basename"
			      "canonicalise" "check_do" "die"
			      "dirname" "do_action" "envvar" "has"
			      "has-version" "highlight"
			      "highlight_warning" "inherit"
			      "is_function" "is_number" "list_libdirs"
			      "load_config" "space" "store_config"
			      "svn_date_to_version""write_error_msg"
			      "write_kv_list_entry" "write_list_start"
			      "write_numbered_list"
			      "write_numbered_list_entry"
			      "write_warning_msg") 'words) 1 'font-lock-type-face))))

(font-lock-add-keywords 'eclectic-mode eclectic-font-lock)

;;;###autoload
(define-derived-mode eclectic-mode shell-script-mode "Eclectic"
  "Major mode for editing files in eclectic format."
  (sh-set-shell "bash")
  (setq fill-column 100
        indent-tabs-mode nil
        require-final-newline t
        tab-width 4))

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.eclectic\\'" . eclectic-mode))

(provide 'eclectic-mode)

;; Local Variables:
;; indent-tabs-mode: nil
;; End:
